common role
===========

Setup a basic Debian system.

Requirements
------------

This role assumes the following:

  - User called `ansible` is present on the system
  - User `ansible` can use sudo without password
  - User `ansible` uses `/bin/sh` compatible shell

Role Variables
--------------

Global variables that need to be present:

- `ssh_keys`: List of ssh public key entries for dropbear server in initramfs and local users
- `dns_servers`: List of fallback DNS servers for systemd-resolved
- `network`: netplan configuration which renders systemd-networkd files

Dependencies
------------

None

Example Playbook
----------------


```yaml
# inventory.yml
---
all:
  hosts:
    server1:
      network:
        bond1:
          dhcp: yes
          member:
            - eno1
            - eno2
    server2:
      network:
        eth0:
          dhcp: yes
```

```yaml
# playbook.yml
---
- hosts: all
  vars:
    ssh_keys:
      - "ssh-ed25519 AAA..."
      - "ssh-rsa AAA..."
    dns_servers:
      - "1.1.1.1"
      - "2606:4700:4700::1111"
  roles:
     - common
```

License
-------

MIT

Author Information
------------------

  - Ricardo (XenGi) Band <email@ricardo.band>

